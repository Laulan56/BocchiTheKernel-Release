# **Bocchi The Kernel For SM8350**

------------

**目录**

- [内核介绍](#内核介绍)
- [主要特性](#主要特性)
- [其他特性](#其他特性)
- [安装方法](#安装方法)
- [相关声明](#相关声明)
- [常见问题](#常见问题)
- [已知问题](#已知问题)
- [下载链接](#下载链接)
- [更新日志](#更新日志)

------------

## 内核介绍
- 适用ROM：MIUI 最新开发版（旧版本可能会出现奇奇怪怪的问题）
- 适用机型：小米11 /Pro/Ultra， Redmi K40 Pro/+

## 主要特性
- 引入了自主开发的温控 Bocchi Thermal，以更加兼顾能耗比的方式调节温控，在提升体验的同时减少发热。

## 其他特性
- 基于CLO 最新内核Tag开发
- ‌清理小米冗余驱动代码仅保留必要驱动
- ‌使用Pandora Clang 15并开启链接时优化
- ‌启用Pandora Clang特有的A78编译器优化
- ‌去除小米驱动日志输出减少功耗
- 屏蔽小米的核心热插拔，Asopt可绑上大核了
- 引入``Bocchi EAS``，优化日用和游戏体验
- 优化F2FS系统的垃圾回收进程
- ‌合并f2fs-stable的linux.5.4.y分支
- ‌从Linux主线反向移植exFAT驱动
- ‌从Linux主线反向移植eroFS更新
- ‌去除小米的垃圾温控
- 屏蔽小米的核心热插拔，asopt可绑上小核了
- ‌优化触控驱动IRQ中断
- ‌内核态震感优化

*更多特性，请下载体验*

## 安装方法
1. 解锁Bootloader
2. 安装内核**卡刷包**
3. 安装Magisk
4. 重启
- **安装完内核第一次重启可能会花费较长时间，请耐心等待！**

## 相关声明
使用此内核具有一定的风险性，包括但不限于：**主板硬件损坏、数据丢失、失去保修等**，本人不承担刷入此内核所带来的任何直接或间接损失和任何后果。若不同意本条款请不要下载使用！一旦内核刷入手机则代表已同意本条款！

## 常见问题
1. 内核需要搭配其他模块使用吗？

	答：如果需要打游戏，推荐配合AsOPT模块使用。本内核内建调度足够优秀，不建议配合其他调度模块使用。

2. 刷了内核还需要刷入devcfg和小米温控删除模块吗？

	答；需要。内核并没有完全移除用户态的小米温控，至于devcfg由于风险太大没有移除。需要用户自己刷入相应模块，才能够获得最好的体验。

## 已知问题
1. 开机提示设备出现问题

	解决方法：修了一版就不能兼容另一版，不修

## 下载链接

- 本内核公开版完全免费，无任何功能上的限制，但公开版更新速度较慢，需要更快体验新功能可以加入内测群，**车费10**。
- 内测群号：436236745（[或点击这里直接加入群聊【BocchiTheKernel 测试群】](https://jq.qq.com/?_wv=1027&k=9NjznDRT "或点击这里直接加入群聊【BocchiTheKernel 测试群】")）
- 捐赠渠道（请在转账中备注您的QQ号以作为捐赠凭证）：
![捐赠渠道](https://i.postimg.cc/gkH2333R/QQ-20230123083554.jpg "捐赠渠道")
- 捐赠完成后，请加管理好友（QQ：526772654或2547121255）或直接在入群问题填您的QQ号或转账订单号，并将捐赠记录发送给管理，管理确认后将拉入群聊。
感谢您的支持！

### 由于学业原因，内核停止更新，我们来年再会！

公开版下载：
- Mi 11: https://wwi.lanzoup.com/b09dpnwfi 密码:7ll8
- Mi 11 Pro/Ultra: https://wwi.lanzoup.com/b09c77yhc 密码:6e50
- Redmi K40 Pro/+: https://wwi.lanzoup.com/b09c77yje 密码:26ub

## 更新日志

### R20 321edb4
别问我为什么说停更还更新一个大版本而且还跳过了R18了！
1. 再次优化日用续航
2. 去除乱七八糟的特性
3. 支持各种各样的内核刷入器，不需要再用TWRP刷入了！（震动滋滋已修复）
4. 史诗级更新：小米11相机修复，正式进版！
……

### R17 5e6767e
别问我为什么说停更还更新一个大版本而且还跳过了R16了！
1. 再次优化日用续航
2. 为小核也增加FEAS
3. 更新perfmgr版本到Pandora Beta10
4. 关闭memcg
5. 关闭devfreq频率限制
6. 启用多线程kswapd
7. 设置ZRAM为物理内存的1.5倍
8. 增加电池温度的刷新频率
……

### R15 bd69e76a
别问我为什么直接跳到R15了！
1. 合并CLO Tag ``LA.UM.9.14.r1-22000-LAHAINA.QSSI12.0`` 各驱动
2. 启用WQ_POWER_EFFICIENT_DEFAULT
3. 关闭kallsyms all 
4. 关闭MMC驱动
5. 关闭无用日志驱动
6. 设置默认qdisc为fq_codel
7. 修复``Xiaomi 11 Pro/Ultra``的双击亮屏功能
……

### R10 dc4d7188
别问我为什么直接跳到R10了！
1. 合并CLO Tag ``LA.UM.9.14.1.r1-08800-QCM6490.QSSI13.0`` 各驱动
2. 把小米的Boost整个扬掉了
3. 换成了系主任牌FBB（Frametime Based Boost），谁用谁说好！
4. 支持exfat了
5. 支持电池解容

### R7 ecaa1007
1. 优化日用耗电
2. 重新配置了CPU升频和GPU升频
3. 优化了LZ4压缩算法
4. 重新加回了millet
5. 更好地兼容了Scene FAS
6. 强制为屏幕启用ULPS超低功耗模式
……

### R4 d271f065
1. 合并最新的``Android Common Kernel``，更新Linux内核小版本到``5.4.230``
2. 合并最新的CLO Tag ``LA.UM.9.14.1.r1-08500-QCM6490.QSSI13.0``
3. 去除无用的勘誤表，优化内核性能
4. 禁用所有可选的CPU缓解措施，优化内核性能
5. 启用O3和LTO编译选项
6. 禁用不必要的驱动和调试选项
7. 引入``Bocchi EAS``，极大降低游戏功耗*
8. 重构CPU BOOST相关逻辑
9. 绑定某些底层线程到小核
10. 调整某些选项使日用功耗得到降低
11. 修复了距离传感器
……

* 效果因人而异

### R3
1. 初次构建。